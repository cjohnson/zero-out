// ANSWERS

//  Level 1 - Splats
ReduceCell(0, 0);
ReduceCell(3, 2);
ReduceCell(3, 2);
ReduceCell(2, 3);

// Level 2 - Line
for (var x = 1; x < 9; x++) {
  ReduceCell(x, 0);
}

// Level 3 - Diagonal
for (var x = 0; x < 10; x++) {
  ReduceCell(9 - x, x);
}

// Level 4 - Uneven
var start = 3;
var end = 0;
for (var y = 0; y < 10; y++) {
  for (var x = start; x < 10 - end; x++) {
    ReduceCell(x, y);
  }
  if (start === 3) {
    start = 0;
    end = 3;
  } else {
    start = 3;
    end = 0;
  }
}

// Level 5 - Hatched Diagonal
for (var y = 0; y < 10; y++) {
  for(var x = y; x < 10; x += 2) {
    ReduceCell(x, y);
  }
}

// Level 6 - Holes
for (var x = 0; x < 10; x++) {
  for (var y = 0; y < 10; y += 2) {
    ReduceCell(x, y);
    ReduceCell(y, x);
  }
}

// Level 7 - Explosion
var startX = 0;
var startY = 0;

for (var ring = 0; ring < 10; ring++) {
  for (var i = 0; i < 9 - ring; i++) {
    var x = startX + ring;
    var y = startY + ring;
    ReduceCell(x, y);
    for (var cellX = 0; cellX < x; cellX++) {
      ReduceCell(cellX, y);
      ReduceCell(y, cellX);
    }
  }
}

// Level 8 - Criss Cross
for (var i = 1; i < 5; i++) {
  for (var x = i; x < 10 - i; x++) {
    ReduceCell(x, i - 1);
    ReduceCell(x, 10 - i);
    ReduceCell(i - 1, x);
    ReduceCell(10 - i, x);
  }
}

// Level 9 - Spiral
var isVertical = true;
var x = 0;
var y = -1;
var offset = 1;
for (var i = 9; i >= 0; i--) {
  for (var j = 0; j < i; j++) {
    if (isVertical) {
      y = y + offset;
    } else {
      x = x + offset;
    }
    ReduceCell(x, y);
  }
  if (i % 2 == 0) {
    offset *= -1;
  }
  isVertical = !isVertical;
}

// Level 10 - Pattern
for (var i = 0; i < 100; i++) {
  var x = i % 10;
  var y = Math.floor(i / 10);
  if ((i + 2) % 4 != 0) {
    ReduceCell(x, y);
  }
}
