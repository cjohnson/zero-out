import {AfterViewInit, Component, NgZone, OnInit, ViewChild} from '@angular/core';

declare const window: any;
declare const Blockly: any;
declare const Interpreter: any;
declare const $: any;

enum ToolboxCategory {
  Logic,
  Loop,
  Math,
  Variables,
  Functions
}

interface Level {
  toolboxCategories: ToolboxCategory[];
  grid: number[][];
  ifCount: number;
  loopCount: number;
  functionCount: number;
  stepLimit: number;
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit, AfterViewInit {
  private static readonly DIV_BLOCKLY = 'blocklyDiv';
  private static readonly DIV_WELCOME_MODAL = '#welcome-modal';

  private static readonly MEDIA_PATH = 'assets/media/';

  private static readonly FUNCTION_HIGHLIGHT_BLOCK = 'highlightBlock';
  private static readonly FUNCTION_HIGHLIGHT_LINE = 'highlightLine';

  private static readonly SFX_FILES_WIN = [
    '/assets/sfx/win.wav',
    '/assets/sfx/win.mp3',
    '/assets/sfx/win.ogg'
  ];
  private static readonly SFX_WIN = 'win';
  private static readonly SFX_FILES_REDUCE = [
    '/assets/sfx/reduce-cell.wav',
    '/assets/sfx/reduce-cell.mp3',
    '/assets/sfx/reduce-cell.ogg'
  ];
  private static readonly SFX_REDUCE = 'reduce-cell';

  private static readonly BLOCK_REDUCE_CELL_NAME = 'ReduceCell';
  private static readonly BLOCK_REDUCE_CELL_DEFINITION = {
    'type': AppComponent.BLOCK_REDUCE_CELL_NAME,
    'message0': `${AppComponent.BLOCK_REDUCE_CELL_NAME} %1 x %2 y %3`,
    'args0': [
      {
        'type': 'input_dummy'
      },
      {
        'type': 'input_value',
        'name': 'x',
        'check': 'Number',
        'align': 'RIGHT'
      },
      {
        'type': 'input_value',
        'name': 'y',
        'check': 'Number',
        'align': 'RIGHT'
      }
    ],
    'inputsInline': true,
    'previousStatement': null,
    'nextStatement': null,
    'colour': 290,
    'tooltip': AppComponent.BLOCK_REDUCE_CELL_NAME,
    'helpUrl': ''
  };

  private static readonly LEVELS = [
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 2, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      ],
      ifCount: 0,
      loopCount: 0,
      functionCount: 4,
      stepLimit: 20,
      name: 'Splats'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      ],
      ifCount: 0,
      loopCount: 1,
      functionCount: 1,
      stepLimit: 100,
      name: 'Line'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      ],
      ifCount: 0,
      loopCount: 1,
      functionCount: 1,
      stepLimit: 100,
      name: 'Diagonal'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0]
      ],
      ifCount: 1,
      loopCount: 2,
      functionCount: 1,
      stepLimit: 350,
      name: 'Uneven'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
        [0, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
        [0, 0, 0, 0, 1, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
        [0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
      ],
      ifCount: 0,
      loopCount: 2,
      functionCount: 1,
      stepLimit: 250,
      name: 'Hatched Diagonal'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
      ],
      ifCount: 0,
      loopCount: 2,
      functionCount: 2,
      stepLimit: 250,
      name: 'Holes'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
        [8, 8, 7, 6, 5, 4, 3, 2, 1, 0],
        [7, 7, 7, 6, 5, 4, 3, 2, 1, 0],
        [6, 6, 6, 6, 5, 4, 3, 2, 1, 0],
        [5, 5, 5, 5, 5, 4, 3, 2, 1, 0],
        [4, 4, 4, 4, 4, 4, 3, 2, 1, 0],
        [3, 3, 3, 3, 3, 3, 3, 2, 1, 0],
        [2, 2, 2, 2, 2, 2, 2, 2, 1, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      ],
      ifCount: 0,
      loopCount: 3,
      functionCount: 3,
      stepLimit: 1000,
      name: 'Explosion'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
        [1, 1, 0, 1, 1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1, 0, 1, 1, 1],
        [1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
        [1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
        [1, 1, 1, 0, 1, 1, 0, 1, 1, 1],
        [1, 1, 0, 1, 1, 1, 1, 0, 1, 1],
        [1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 0]
      ],
      ifCount: 0,
      loopCount: 2,
      functionCount: 4,
      stepLimit: 250,
      name: 'Criss Cross'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 1, 1, 1, 1, 1, 1, 0],
        [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
        [1, 0, 1, 0, 1, 1, 1, 0, 1, 0],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        [1, 0, 1, 0, 0, 0, 1, 0, 1, 0],
        [1, 0, 1, 1, 1, 1, 1, 0, 1, 0],
        [1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      ],
      ifCount: 2,
      loopCount: 2,
      functionCount: 1,
      stepLimit: 700,
      name: 'Spiral'
    },
    <Level>{
      toolboxCategories: [
        ToolboxCategory.Logic, ToolboxCategory.Loop, ToolboxCategory.Math,
        ToolboxCategory.Variables, ToolboxCategory.Functions
      ],
      grid: [
        [1, 1, 0, 1, 1, 1, 0, 1, 1, 1],
        [0, 1, 1, 1, 0, 1, 1, 1, 0, 1],
        [1, 1, 0, 1, 1, 1, 0, 1, 1, 1],
        [0, 1, 1, 1, 0, 1, 1, 1, 0, 1],
        [1, 1, 0, 1, 1, 1, 0, 1, 1, 1],
        [0, 1, 1, 1, 0, 1, 1, 1, 0, 1],
        [1, 1, 0, 1, 1, 1, 0, 1, 1, 1],
        [0, 1, 1, 1, 0, 1, 1, 1, 0, 1],
        [1, 1, 0, 1, 1, 1, 0, 1, 1, 1],
        [0, 1, 1, 1, 0, 1, 1, 1, 0, 1]
      ],
      ifCount: 1,
      loopCount: 1,
      functionCount: 1,
      stepLimit: 700,
      name: 'Pattern'
    },
  ];

  private static readonly BLOCKLY_INITIAL = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="ReduceCell" id="pkfi)A!}xln9+,8oN,[#" x="27" y="63"><value name="x"><shadow type="math_number" id="mnI}WArxT@i:z-@s*fPU"><field name="NUM">0</field></shadow></value><value name="y"><shadow type="math_number" id="$h11ZxGp,sH(VdP9e;jj"><field name="NUM">0</field></shadow></value></block></xml>';
  private static readonly BLOCKLY_EMPTY = '<xml xmlns="http://www.w3.org/1999/xhtml"><variables></variables></xml>';
  private static readonly ACE_CODE_INITIAL = `// Code is written in Javascript.\n// Looking at the Javascript Reference is recommended\n\n// NOTE: Loops and if statements must follow style used in JS Reference.\n\n${AppComponent.BLOCK_REDUCE_CELL_NAME}(0, 0);`;
  private static readonly ACE_CODE_INITAL_ALL = '// NOTE: Loops and if statements must follow style used in JS Reference.\n';

  public demoCodeOperations =
    'var a = 5 + 2;   // a = 10\n' +
    'var b = 5 - 3;   // b = 2\n' +
    'var c = 5 * 3;   // c = 15\n' +
    'var d = 10 / 2;  // d = 2.5\n' +
    'var e = 12 % 5;  // e = 2\n' +
    'var f = ! true;  // f = false';

  public demoCodeIf =
    'if (x > 0) {\n' +
    '    // ...\n' +
    '}';

  public demoCodeLoop =
    '// Loop values 1, 2, 3, 4\n' +
    'for (var x = 1; x < 5; x++) {\n' +
    '    // ...\n' +
    '}\n' +
    '\n' +
    'var x = 0;\n' +
    'do {\n' +
    '    x++;\n' +
    '    // ...\n' +
    '} while (x < 5);';

  public demoCodeFunction =
    '// Decreases value in cell 3, 5 by 1\n' +
    '// x and y must be between 0 - 9\n' +
    'var x = 3;\n' +
    'var y = 5;\n' +
    'ReduceCell(x, y);\n' +
    '// Don\'t that ReduceCell requires integer arguments\n' +
    'var g = Math.floor(1.25); // g = 1\n' +
    'var h = Math.ceil(1.25);  // h = 1\n' +
    '\n' +
    '// Creating your own function isn\'t\n' +
    '// necessary to solve these problems';

  public aceMode = 'typescript';
  public aceTheme = 'eclipse';
  public aceOptions: any = {
    maxLines: 30,
    printMargin: false
  };
  public aceDemoOptions: any = {
    maxLines: 30,
    printMargin: false ,
    highlightActiveLine: false,
    highlightSelectedWord: false,
    behavioursEnabled: false,
    readOnly: false,
    highlightGutterLine: false
  };
  public aceGenerateBlocklyOptions: any = {
    maxLines: 13,
    printMargin: false ,
    highlightActiveLine: false,
    highlightSelectedWord: false,
    behavioursEnabled: false,
    readOnly: false,
    highlightGutterLine: false
  };

  private _currentLevelIdx = 0;
  public usedIfs = 0;
  public usedLoops = 0;
  public usedFunctions = 0;
  public grid: number[][];

  public isShowingBlockly = true;

  private _blocklyWorkspace: any;

  private _blocklyXMLPerLevel: string[] = [];

  @ViewChild('aceEditor') aceEditor: any;
  private _aceCodePerLevel: string[] = [];

  private _interpreter: any;
  public consoleMessage = '';
  public blocklyJSCode = '';

  private _isHighlightPaused = false;
  public isRunningCode = false;
  private _timerCode = undefined;

  private _remainingSteps = 0;

  public stepSpeeds = [
    { label: 'Slow', value: 750 },
    { label: 'Medium', value: 500 },
    { label: 'Fast', value: 150 },
    { label: 'Very Fast', value: 50 },
    { label: 'Turbo', value: 20 },
  ];
  public stepInterval = this.stepSpeeds[1].value;

  private _hasLevelBeenFinished: boolean[] = [];
  public isShowingJSReference = false;

  private _willSkipTutorial = false;
  private _popoverDivs = [ '#btn-loop', '#code-column', '#btn-run', '#btn-reset', '#btn-mode' ];
  private _currentPopoverIdx = -1;

  constructor(private _zone: NgZone) {
    (window as any).angularComponentRef = {component: this, zone: _zone};

    for (let idx = 0; idx < AppComponent.LEVELS.length; idx++) {
      this._hasLevelBeenFinished.push(false);
      this._aceCodePerLevel.push(AppComponent.ACE_CODE_INITAL_ALL);
      this._blocklyXMLPerLevel.push(AppComponent.BLOCKLY_EMPTY);
    }
  }

  ngOnInit() {
    (window as any).nextTutorialPopup = () => {
      this.showNextTutorialPopup();
    };

    Blockly.Blocks[AppComponent.BLOCK_REDUCE_CELL_NAME] = {
      init: function () {
        this.jsonInit(AppComponent.BLOCK_REDUCE_CELL_DEFINITION);
      }
    };

    Blockly.JavaScript.STATEMENT_PREFIX = `${AppComponent.FUNCTION_HIGHLIGHT_BLOCK}(%1);\n`;
    Blockly.JavaScript.addReservedWords(AppComponent.FUNCTION_HIGHLIGHT_BLOCK);

    Blockly.JavaScript[AppComponent.BLOCK_REDUCE_CELL_NAME] = (block) => {
      const x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
      const y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
      return `${AppComponent.BLOCK_REDUCE_CELL_NAME}(${x}, ${y});\n`;
    };

    this._blocklyWorkspace = Blockly.inject(AppComponent.DIV_BLOCKLY, {
      media: AppComponent.MEDIA_PATH,
      toolbox: this.generateToolboxXML([
        ToolboxCategory.Logic,
        ToolboxCategory.Loop,
        ToolboxCategory.Math,
        ToolboxCategory.Variables,
        ToolboxCategory.Functions
      ]),
      scrollbars: true
    });

    const audioManager = this._blocklyWorkspace.getAudioManager();
    audioManager.load(AppComponent.SFX_FILES_WIN, AppComponent.SFX_WIN);
    audioManager.load(AppComponent.SFX_FILES_REDUCE, AppComponent.SFX_REDUCE);

    this.setLevel(0);

    this.aceCode = AppComponent.ACE_CODE_INITIAL;
    this.blocklyXML = AppComponent.BLOCKLY_INITIAL;
    this.setBlocksXML(AppComponent.BLOCKLY_INITIAL);

    this._blocklyWorkspace.addChangeListener((event) => {
      const code = Blockly.JavaScript.workspaceToCode(this._blocklyWorkspace);
      this.blocklyJSCode = code.replace(/^\s*highlightBlock.*\n/gm, '');

      if (event instanceof Blockly.Events.Create || event instanceof Blockly.Events.Delete || event instanceof Blockly.Events.Change) {
        this.refreshBlocklyCounts();
      }

      this.blocklyXML = this.getBlocksXML();
    });

    this.aceEditor.getEditor().session.on('change', (delta) => {
      this.refreshAceCounts();
    });
  }

  ngAfterViewInit() {
    $(AppComponent.DIV_WELCOME_MODAL).modal({});
    $(AppComponent.DIV_WELCOME_MODAL).on('hidden.bs.modal', (e) => {
      if (this._willSkipTutorial) {
        return;
      }

      this.showNextTutorialPopup();
    });
  }

  public showNextTutorialPopup() {
    if (this._currentPopoverIdx >= 0) {
      $(this._popoverDivs[this._currentPopoverIdx]).popover('hide');
    }

    this._currentPopoverIdx++;

    if (this._currentPopoverIdx < this._popoverDivs.length) {
      const div = this._popoverDivs[this._currentPopoverIdx];
      $(div).popover('show');
      $(div).popover('disable');
    }
  }

  public skipTutorial() {
    this._willSkipTutorial = true;
  }

  private refreshAceCounts() {
    this.updateRestrictionCounts(this.aceCode);
  }

  private refreshBlocklyCounts() {
    this.updateRestrictionCounts(this.blocklyJSCode);
  }

  private updateRestrictionCounts(code: string) {
    const reduceCellMatches = code.match(/ReduceCell *\(/g);
    this.usedFunctions = reduceCellMatches ? reduceCellMatches.length : 0;

    const ifMatches = code.match(/if *\(/g);
    const ternaryMatches = code.match(/\?/g);
    this.usedIfs = (ifMatches ? ifMatches.length : 0) + (ternaryMatches ? ternaryMatches.length : 0);

    const forMatches = code.match(/for *\(/g);
    const whileMatches = code.match(/while *\(/g);
    this.usedLoops = (forMatches ? forMatches.length : 0) + (whileMatches ? whileMatches.length : 0);
  }

  private setLevel(levelIdx: number) {
    if (levelIdx < 0 || levelIdx >= AppComponent.LEVELS.length) {
      return;
    }

    this._currentLevelIdx = levelIdx;
    this.setBlocksXML(this.blocklyXML);

    this.grid = [];
    this.resetGrid();

    this.refreshToolbox();
  }

  public get ifCount(): number {
    return AppComponent.LEVELS[this._currentLevelIdx].ifCount;
  }

  public get loopCount(): number {
    return AppComponent.LEVELS[this._currentLevelIdx].loopCount;
  }

  public get functionCount(): number {
    return AppComponent.LEVELS[this._currentLevelIdx].functionCount;
  }

  public get stepLimit(): number {
    return AppComponent.LEVELS[this._currentLevelIdx].stepLimit;
  }

  public get hasLevelBeenFinished() {
    return this._hasLevelBeenFinished[this._currentLevelIdx];
  }

  public get isFirstLevel(): boolean {
    return this._currentLevelIdx === 0;
  }

  public get isLastLevel(): boolean {
    return this._currentLevelIdx === AppComponent.LEVELS.length - 1;
  }

  public previousLevel() {
    this.setLevel(this._currentLevelIdx - 1);
  }

  public nextLevel() {
    this.setLevel(this._currentLevelIdx + 1);
  }

  public toggleIsShowingJSReference() {
    this.isShowingJSReference = !this.isShowingJSReference;
  }

  public get levelName() {
    const currentLevelName = AppComponent.LEVELS[this._currentLevelIdx].name;
    return `${currentLevelName} - Level ${this._currentLevelIdx + 1} / ${AppComponent.LEVELS.length}`;
  }

  public get aceCode() {
    return this._aceCodePerLevel[this._currentLevelIdx];
  }

  public set aceCode(code: string) {
    this._aceCodePerLevel[this._currentLevelIdx] = code;
  }

  public get blocklyXML() {
    return this._blocklyXMLPerLevel[this._currentLevelIdx];
  }

  public set blocklyXML(xml: string) {
    this._blocklyXMLPerLevel[this._currentLevelIdx] = xml;
  }

  public resetStepUI() {
    this._blocklyWorkspace.highlightBlock(null);
    this._isHighlightPaused = false;
    if (this._timerCode !== undefined) {
      clearInterval(this._timerCode);
      this._timerCode = undefined;
    }
  }

  public runCode() {
    if (this._currentPopoverIdx === 2) {
      $(this._popoverDivs[this._currentPopoverIdx]).popover('hide');
      setTimeout(() => {
        this.showNextTutorialPopup();
      }, 1000);
    }

    if (this.isRunningCode) {
      return;
    }
    this.isRunningCode = true;

    this.stepCode();
    if (this.isRunningCode) {
      this._timerCode = setInterval(() => {
        this.stepCode();
      }, this.stepInterval);
    }
  }

  public stopCode() {
    this.onRunFinished();
  }

  private stepCode() {
    if (!this._interpreter) {
      this.resetStepUI();
      this.resetGrid();
      this._remainingSteps = this.stepLimit;
      this.consoleMessage = '';

      if (this.isShowingBlockly) {
        // Catch ReduceCell blocks with empty connections
        const blocks = this._blocklyWorkspace.getAllBlocks();
        for (const block of blocks) {
          if (block.type === AppComponent.BLOCK_REDUCE_CELL_NAME &&
            (!block.getInputTargetBlock('x') || !block.getInputTargetBlock('y'))) {
            this.consoleMessage = 'ReduceCell block missing arguments';
            this.isShowingJSReference = false;
            this.onRunFinished();
            return;
          }
        }
      } else {
        // Catch JS errors early and before line highlight function is added
        try {
          const interpreter = new Interpreter(this.aceCode, (interp, scope) => {
            interp.setProperty(scope, 'alert', interp.createNativeFunction((text) => {
            }));

            interp.setProperty(scope, 'prompt', interp.createNativeFunction((text) => {
            }));

            interp.setProperty(scope, AppComponent.BLOCK_REDUCE_CELL_NAME, interp.createNativeFunction((xArg, yArg) => {
            }));
          });

          let hasMoreCode = true;
          let steps = this.stepLimit;

          do {
            if (steps <= 0) {
              hasMoreCode = false;
              continue;
            }

            try {
              hasMoreCode = interpreter.step();
            } catch (error) {
              this.consoleMessage = error;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }
            steps--;
          } while (hasMoreCode);
        } catch (error) {
          this.consoleMessage = error;
          this.isShowingJSReference = false;
          this.onRunFinished();
          return;
        }
      }

      let code = '';
      if (this.isShowingBlockly) {
        code = Blockly.JavaScript.workspaceToCode(this._blocklyWorkspace);
      } else {
        let formattedAceCode = '';
        const lines = this.aceCode.split('\n');
        for (let idx = 0; idx < lines.length; idx++) {
          formattedAceCode += `${AppComponent.FUNCTION_HIGHLIGHT_LINE}(${idx + 1});\n`;
          formattedAceCode += lines[idx] + '\n';
        }
        code = formattedAceCode;
      }

      try {
        this._interpreter = new Interpreter(code, (interpreter, scope) => {
          interpreter.setProperty(scope, 'alert', interpreter.createNativeFunction((text) => {
          }));

          interpreter.setProperty(scope, 'prompt', interpreter.createNativeFunction((text) => {
            text = text ? text.toString() : '';
            return interpreter.createPrimitive(prompt(text));
          }));

          interpreter.setProperty(scope, AppComponent.FUNCTION_HIGHLIGHT_BLOCK, interpreter.createNativeFunction((blockIdArg) => {
            if (!blockIdArg) {
              this.consoleMessage = `${AppComponent.FUNCTION_HIGHLIGHT_BLOCK} - Missing block id argument`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            if (blockIdArg.type !== 'string') {
              this.consoleMessage = `${AppComponent.FUNCTION_HIGHLIGHT_BLOCK} - block id argument is not a string`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            this._blocklyWorkspace.highlightBlock(blockIdArg.toString());
            this._isHighlightPaused = true;
          }));

          interpreter.setProperty(scope, AppComponent.FUNCTION_HIGHLIGHT_LINE, interpreter.createNativeFunction((lineNumberArg) => {
            if (!lineNumberArg) {
              this.consoleMessage = `${AppComponent.FUNCTION_HIGHLIGHT_LINE} - Missing line number argument`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            if (lineNumberArg.type !== 'number') {
              this.consoleMessage = `${AppComponent.FUNCTION_HIGHLIGHT_LINE} - Line number argument is not number`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            const lineNumber = lineNumberArg.toNumber();
            this.aceEditor.getEditor().gotoLine(lineNumber);
            this._isHighlightPaused = true;
          }));

          interpreter.setProperty(scope, AppComponent.BLOCK_REDUCE_CELL_NAME, interpreter.createNativeFunction((xArg, yArg) => {
            if (!xArg) {
              this.consoleMessage = `${AppComponent.BLOCK_REDUCE_CELL_NAME} - Missing x argument`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            if (xArg.type !== 'number') {
              this.consoleMessage = `${AppComponent.BLOCK_REDUCE_CELL_NAME} - x argument must be number`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            if (!yArg) {
              this.consoleMessage = `${AppComponent.BLOCK_REDUCE_CELL_NAME} - Missing y argument`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            if (yArg.type !== 'number') {
              this.consoleMessage = `${AppComponent.BLOCK_REDUCE_CELL_NAME} - y argument must be number`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            const x = xArg.toNumber();
            const y = yArg.toNumber();

            if (x % 1 > 0.0 || y % 1 > 0.0) {
              this.consoleMessage = `${AppComponent.BLOCK_REDUCE_CELL_NAME} - Numbers must be integers (Use Math.floor or Math.ceil)`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            if (y < 0 || y >= this.grid.length || x < 0 || x >= this.grid[y].length) {
              this.consoleMessage = `ReduceCell(${x}, ${y}) - Out of bounds`;
              this.isShowingJSReference = false;
              this.onRunFinished();
              return;
            }

            this.grid[y][x]--;

            const audioManager = this._blocklyWorkspace.getAudioManager();
            audioManager.play(AppComponent.SFX_REDUCE);
          }));
        });
      } catch (error) {
        this.consoleMessage = error;
        this.isShowingJSReference = false;
        this.onRunFinished();
        return;
      }
    }

    if (this._remainingSteps <= 0) {
      this.consoleMessage = 'Reached Step Limit';
      this.isShowingJSReference = false;
      this.onRunFinished();
      return;
    }

    this._isHighlightPaused = false;
    let hasMoreCode = true;

    do {
      try {
        hasMoreCode = this._interpreter.step();
      } catch (error) {
        this.consoleMessage = error;
        this.isShowingJSReference = false;
        this.onRunFinished();
      } finally {
        if (!hasMoreCode) {
          this.onRunFinished();
          this.aceEditor.getEditor().gotoLine(0);
        }
      }
    } while (hasMoreCode && !this._isHighlightPaused && this._interpreter);

    this._remainingSteps--;

    const currentLevel = AppComponent.LEVELS[this._currentLevelIdx];
    if (this._interpreter === undefined &&
      this.usedFunctions <= currentLevel.functionCount &&
      this.usedIfs <= currentLevel.ifCount &&
      this.usedLoops <= currentLevel.loopCount &&
      this.isGridZeroed()) {
      this._hasLevelBeenFinished[this._currentLevelIdx] = true;
      this.consoleMessage = 'Level Complete';
      this.isShowingJSReference = false;

      // Delay so that grid reduce sfx has a change to finish
      setTimeout(() => {
        const audioManager = this._blocklyWorkspace.getAudioManager();
        audioManager.play(AppComponent.SFX_WIN);
      }, 100);
    }
  }

  private onRunFinished() {
    this._interpreter = undefined;
    this.isRunningCode = false;
    this.resetStepUI();
  }

  private isGridZeroed(): boolean {
    for (let rowIdx = 0; rowIdx < this.grid.length; rowIdx++) {
      const row = this.grid[rowIdx];
      for (let colIdx = 0; colIdx < row.length; colIdx++) {
        if (row[colIdx] !== 0) {
          return false;
        }
      }
    }
    return true;
  }

  public resetGrid() {
    if (this._currentPopoverIdx === 3) {
      this.showNextTutorialPopup();
    }

    const currentLevel: Level = AppComponent.LEVELS[this._currentLevelIdx];
    this.grid = [];

    for (let rowIdx = 0; rowIdx < currentLevel.grid.length; rowIdx++) {
      const levelRow = [];
      const row = currentLevel.grid[rowIdx];
      for (let colIdx = 0; colIdx < row.length; colIdx++) {
        levelRow.push(row[colIdx]);
      }
      this.grid.push(levelRow);
    }
  }

  public refreshToolbox() {
    const currentLevel: Level = AppComponent.LEVELS[this._currentLevelIdx];
    this.updateToolbox(currentLevel.toolboxCategories);
  }

  updateToolbox(categories: ToolboxCategory[]) {
    this._blocklyWorkspace.updateToolbox(this.generateToolboxXML(categories));
  }

  public toggleIsShowingBlockly() {
    this.isShowingBlockly = !this.isShowingBlockly;
    this.consoleMessage = '';
    if (this.isShowingBlockly) {
      this.refreshBlocklyCounts();
    } else {
      this.refreshAceCounts();
    }
  }

  generateToolboxXML(categories: ToolboxCategory[]): string {
    let toolboxXML = '<xml>';
    for (const category of categories) {
      switch (category) {
        case ToolboxCategory.Logic:
          toolboxXML += '<category name="Logic" colour="210">';
          toolboxXML += '<block type="controls_if"></block>';
          toolboxXML += '<block type="controls_if"><mutation else="1"></mutation></block>';
          toolboxXML += '<block type="logic_compare"></block>';
          toolboxXML += '<block type="logic_operation"></block>';
          toolboxXML += '<block type="logic_negate"></block>';
          toolboxXML += '<block type="logic_boolean"></block>';
          toolboxXML += '</category>';
          break;
        case ToolboxCategory.Loop:
          toolboxXML += '<category name="Loops" colour="120">';
          toolboxXML += '<block type="controls_for">';
          toolboxXML += '<value name="FROM">';
          toolboxXML += '<shadow type="math_number">';
          toolboxXML += '<field name="NUM">1</field>';
          toolboxXML += '</shadow>';
          toolboxXML += '</value>';
          toolboxXML += '<value name="TO" >';
          toolboxXML += '<shadow type="math_number">';
          toolboxXML += '<field name="NUM">10</field>';
          toolboxXML += '</shadow>';
          toolboxXML += '</value>';
          toolboxXML += '<value name="BY">';
          toolboxXML += '<shadow type="math_number">';
          toolboxXML += '<field name="NUM">1</field>';
          toolboxXML += '</shadow>';
          toolboxXML += '</value>';
          toolboxXML += '</block>';
          toolboxXML += '<block type="controls_repeat_ext">';
          toolboxXML += '<value name="TIMES">';
          toolboxXML += '<shadow type="math_number">';
          toolboxXML += '<field name="NUM">10</field>';
          toolboxXML += '</shadow>';
          toolboxXML += '</value>';
          toolboxXML += '</block>';
          toolboxXML += '<block type="controls_whileUntil"></block>';
          toolboxXML += '</category>';
          break;
        case ToolboxCategory.Math:
          toolboxXML += '<category name="Math" colour="230">';
          toolboxXML += '<block type="math_number"></block>';
          toolboxXML += '<block type="math_arithmetic">';
          toolboxXML += '<value name="A"><shadow type="math_number"><field name="NUM">1</field></shadow></value>';
          toolboxXML += '<value name="B"><shadow type="math_number"><field name="NUM">1</field></shadow></value>';
          toolboxXML += '</block>';
          toolboxXML += '<block type="math_modulo">';
          toolboxXML += '<value name="DIVIDEND"><shadow type="math_number"><field name="NUM">1</field></shadow></value>';
          toolboxXML += '<value name="DIVISOR"><shadow type="math_number"><field name="NUM">1</field></shadow></value>';
          toolboxXML += '</block>';
          toolboxXML += '<block type="math_number_property">';
          toolboxXML += '<value name="NUMBER_TO_CHECK"><shadow type="math_number"><field name="NUM">0</field></shadow></value>';
          toolboxXML += '</block>';
          toolboxXML += '<block type="math_round"></block>';
          toolboxXML += '</category>';
          break;
        case ToolboxCategory.Variables:
          toolboxXML += '<category name="Variables" custom="VARIABLE" colour="330"></category>';
          break;
        case ToolboxCategory.Functions:
          toolboxXML += '<category name="Functions" colour="290">';
          toolboxXML += `<block type="${AppComponent.BLOCK_REDUCE_CELL_NAME}">`;
          toolboxXML += '<value name="x"><shadow type="math_number"><field name="NUM">0</field></shadow></value>';
          toolboxXML += '<value name="y"><shadow type="math_number"><field name="NUM">0</field></shadow></value>';
          toolboxXML += '</block>';
          toolboxXML += '</category>';
          break;
        default:
      }
    }
    toolboxXML += '</xml>';
    return toolboxXML;
  }

  private getBlocksXML() {
    return Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(this._blocklyWorkspace));
  }

  private setBlocksXML(xml: string) {
    this._blocklyWorkspace.clear();
    const blockXMLDom = Blockly.Xml.textToDom(xml);
    Blockly.Xml.domToWorkspace(blockXMLDom, this._blocklyWorkspace);
  }
}
